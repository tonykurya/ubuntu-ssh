#!/bin/sh
# Update the keys stored in ssh.yaml from the authorized-keys file
kubectl --namespace sshd delete secret ssh-keys
kubectl --namespace sshd create secret generic ssh-keys --from-file=./authorized_keys
